# Vars
VAULT=docker exec -it vault

# Docker
vault: vault-compose alias

vault-compose-up:
	cd compose && docker-compose up -d

vault-run:
	cd run && bash vault-run.sh


# Docker
up: init unseal auth

init:
	${VAULT} vault init -key-shares=3 -key-threshold=2

unseal:
	${VAULT} vault unseal ${VAULT_KEY_1} && ${VAULT} vault unseal ${VAULT_KEY_2}

auth:
	${VAULT} vault auth ${VAULT_TOKEN}


# Write secrets
write: gitlab-private-token gitlab-hhvm-runner-token gitlab-nginx-runner-token gitlab-percona-runner-token gitlab-php-fpm-runner-token gitlab-redis-runner-token gitlab-website-runner-token mysql-root-password website-name admin-email db-user-password wp-user wp-user-password

gitlab-private-token:
	${VAULT} vault write secret/gitlab-private-token value=${GITLAB_PRIVATE_TOKEN}

gitlab-hhvm-runner-token:
	${VAULT} vault write secret/gitlab-hhvm-runner-token value=${GITLAB_HHVM_RUNNER_TOKEN}

gitlab-nginx-runner-token:
	${VAULT} vault write secret/gitlab-nginx-runner-token value=${GITLAB_NGINX_RUNNER_TOKEN}

gitlab-percona-runner-token:
	${VAULT} vault write secret/gitlab-percona-runner-token value=${GITLAB_PERCONA_RUNNER_TOKEN}

gitlab-php-fpm-runner-token:
	${VAULT} vault write secret/gitlab-php-fpm-runner-token value=${GITLAB_PHP_FPM_RUNNER_TOKEN}

gitlab-redis-runner-token:
	${VAULT} vault write secret/gitlab-redis-runner-token value=${GITLAB_REDIS_RUNNER_TOKEN}

gitlab-website-runner-token:
	${VAULT} vault write secret/gitlab-website-runner-token value=${GITLAB_WEBSITE_RUNNER_TOKEN}

mysql-root-password:
	${VAULT} vault write secret/mysql-root-password value=${MYSQL_ROOT_PASSWORD}

website-name:
	${VAULT} vault write secret/website-name value=${WEBSITE_NAME}

admin-email:
	${VAULT} vault write secret/admin-email value=${ADMIN_EMAIL}

db-user-password:
	${VAULT} vault write secret/db-user-password value=${DB_USER_PASSWORD}

wp-user:
	${VAULT} vault write secret/wp-user value=${WP_USER}

wp-user-password:
	${VAULT} vault write secret/wp-user-password value=${WP_USER_PASSWORD}
